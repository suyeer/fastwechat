package com.suyeer.fastwechat.bean.fwcommon;

import com.suyeer.basic.util.DateUtil;
import com.suyeer.basic.util.BasicUtil;
import com.suyeer.fastwechat.util.FwConstUtil;

/**
 * @author jun 2018/11/22
 */
public class JsSdkConfig {
    private String appId;
    private String timestamp;
    private String nonceStr;
    private String signature;

    public JsSdkConfig() {
        appId = FwConstUtil.FW_APP_ID;
        timestamp = DateUtil.getTimestampString();
        nonceStr = BasicUtil.getNonceStr();
    }

    public String getAppId() {
        return appId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
