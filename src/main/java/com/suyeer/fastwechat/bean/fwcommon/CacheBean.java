package com.suyeer.fastwechat.bean.fwcommon;

import com.alibaba.fastjson.annotation.JSONField;
import com.suyeer.fastwechat.util.FwBaseUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jun 2018/11/22
 */
public class CacheBean implements Serializable {
    private String key;
    private String value;
    private Date createTime;
    private long timestamp;
    private long expiredIn;

    public CacheBean(String key, String value, long expiredIn) {
        this.key = key;
        this.value = value;
        this.createTime = new Date();
        this.expiredIn = expiredIn;
        this.timestamp = this.createTime.getTime() / 1000;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getExpiredIn() {
        return expiredIn;
    }

    public void setExpiredIn(long expiredIn) {
        this.expiredIn = expiredIn;
    }

    @JSONField(serialize = false)
    public Boolean isNotExpired() {
        return (timestamp + expiredIn) > FwBaseUtil.getTimestamp();
    }
}
