package com.suyeer.fastwechat.bean.fwmenu;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jun 2018/11/22
 */
public class FwMenuBean {
    private List<FwButtonBean> button;

    public FwMenuBean(){
        button = new ArrayList<>();
    }

    public List<FwButtonBean> getButton() {
        return button;
    }

    public void setButton(List<FwButtonBean> button) {
        this.button = button;
    }
}
