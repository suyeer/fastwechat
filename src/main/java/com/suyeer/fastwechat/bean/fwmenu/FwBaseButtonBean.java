package com.suyeer.fastwechat.bean.fwmenu;

import com.suyeer.fastwechat.enums.MenuButtonTypeEnum;

/**
 * @author jun 2018/11/22
 */
public class FwBaseButtonBean {
    private String name;
    private MenuButtonTypeEnum type;
    private String key;
    private String url;
    private String media_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuButtonTypeEnum getType() {
        return type;
    }

    public void setType(MenuButtonTypeEnum type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }
}
