package com.suyeer.fastwechat.bean.fwmenu;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jun 2018/11/22
 */
public class FwButtonBean extends FwBaseButtonBean {
    private List<FwSubButtonBean> sub_button;

    public FwButtonBean(){
        sub_button = new ArrayList<>();
    }


    public List<FwSubButtonBean> getSub_button() {
        return sub_button;
    }

    public void setSub_button(List<FwSubButtonBean> sub_button) {
        this.sub_button = sub_button;
    }

}
