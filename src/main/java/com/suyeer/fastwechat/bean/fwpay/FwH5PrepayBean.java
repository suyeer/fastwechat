package com.suyeer.fastwechat.bean.fwpay;

import com.suyeer.basic.util.BasicUtil;
import com.suyeer.fastwechat.util.FwConstUtil;
import com.suyeer.fastwechat.util.FwPayUtil;

/**
 * 微信H5支付bean, 参数命名遵循微信开发文档中的格式
 *
 * @author jun 2018/11/22
 */
public class FwH5PrepayBean {
    private String appid;
    private String mch_id;
    private String mch_key;
    private String nonce_str;
    private String body;
    private String out_trade_no;
    private String total_fee;
    private String spbill_create_ip;
    private String trade_type;
    private String notify_url;
    private String scene_info;

    public FwH5PrepayBean(String userIp, String body, Integer totalFee, String notifyUrl, String outTradeNoPreFix) throws Exception {
        this.appid = FwConstUtil.FW_APP_ID;
        this.mch_id = FwConstUtil.FW_MCH_ID;
        this.mch_key = FwConstUtil.FW_PARTNER_KEY;
        this.nonce_str = BasicUtil.getNonceStr();
        this.out_trade_no = FwPayUtil.getOrderNumber(outTradeNoPreFix);
        this.body = body;
        this.total_fee = String.valueOf(totalFee);
        this.spbill_create_ip = userIp;
        this.trade_type = FwConstUtil.H5_TRADE_TYPE;
        this.notify_url = notifyUrl;
    }

    public String getAppid() {
        return appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public String getBody() {
        return body;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public String getScene_info() {
        return scene_info;
    }

    public void setScene_info(String scene_info) {
        this.scene_info = scene_info;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getMch_key() {
        return mch_key;
    }

    public void setMch_key(String mch_key) {
        this.mch_key = mch_key;
    }
}
