package com.suyeer.fastwechat.bean.fwpay;

import com.alibaba.fastjson.JSON;

/**
 * 微信 H5 统一下单结果数据, 变量名命名规范遵循微信返回的xml中的字段命名
 *
 * @author jun 2019/3/21
 */
public class FwH5PayUnifiedOrderResult extends FwPayBasicResult{

    private String mweb_url;

    public String getMweb_url() {
        return mweb_url;
    }

    public void setMweb_url(String mweb_url) {
        this.mweb_url = mweb_url;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}
