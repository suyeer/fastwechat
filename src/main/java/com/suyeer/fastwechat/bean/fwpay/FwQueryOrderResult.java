package com.suyeer.fastwechat.bean.fwpay;

import com.alibaba.fastjson.JSON;

/**
 * @author jun 2019/2/26
 */
public class FwQueryOrderResult extends FwPayNotifyResult {

    private String attach;
    private String trade_state;
    private String trade_state_desc;

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getTrade_state() {
        return trade_state;
    }

    public void setTrade_state(String trade_state) {
        this.trade_state = trade_state;
    }

    public String getTrade_state_desc() {
        return trade_state_desc;
    }

    public void setTrade_state_desc(String trade_state_desc) {
        this.trade_state_desc = trade_state_desc;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
