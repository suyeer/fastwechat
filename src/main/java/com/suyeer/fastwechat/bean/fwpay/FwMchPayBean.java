package com.suyeer.fastwechat.bean.fwpay;

import com.suyeer.basic.util.BasicUtil;
import com.suyeer.fastwechat.util.FwConstUtil;
import com.suyeer.fastwechat.util.FwPayUtil;

/**
 * 企业付款用bean, 参数命名遵循微信开发文档中的格式
 *
 * @author jun 2019/1/17
 */
public class FwMchPayBean {
    private String mch_appid;
    private String mchid;
    private String mch_key;
    private String nonce_str;
    private String partner_trade_no;
    private String openid;
    private String check_name;
    private String amount;
    private String desc;
    private String spbill_create_ip;

    public FwMchPayBean(String userIp, String openId, Integer amount, String desc, String outTradeNoPreFix) {
        this.mch_appid = FwConstUtil.FW_APP_ID;
        this.mchid = FwConstUtil.FW_MCH_ID;
        this.mch_key = FwConstUtil.FW_PARTNER_KEY;
        this.nonce_str = BasicUtil.getNonceStr();
        this.partner_trade_no = FwPayUtil.getOrderNumber(outTradeNoPreFix);
        this.openid = openId;
        this.check_name = "NO_CHECK";
        this.amount = String.valueOf(amount);
        this.desc = desc;
        this.spbill_create_ip = userIp;
    }

    public String getMch_appid() {
        return mch_appid;
    }

    public String getMchid() {
        return mchid;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public String getPartner_trade_no() {
        return partner_trade_no;
    }

    public String getOpenid() {
        return openid;
    }

    public String getCheck_name() {
        return check_name;
    }

    public String getAmount() {
        return amount;
    }

    public String getDesc() {
        return desc;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setCheck_name(String check_name) {
        this.check_name = check_name;
    }

    public void setMch_appid(String mch_appid) {
        this.mch_appid = mch_appid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getMch_key() {
        return mch_key;
    }

    public void setMch_key(String mch_key) {
        this.mch_key = mch_key;
    }
}
