package com.suyeer.fastwechat.bean.fwpay;

import com.suyeer.basic.util.BasicUtil;
import com.suyeer.fastwechat.util.FwConstUtil;
import com.suyeer.fastwechat.util.FwPayUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * 微信支付bean, 参数命名遵循微信开发文档中的格式
 *
 * @author jun 2018/11/22
 */
public class FwPrepayBean {
    private String appid;
    private String openid;
    private String mch_id;
    private String mch_key;
    private String nonce_str;
    private String body;
    private String out_trade_no;
    private String total_fee;
    private String spbill_create_ip;
    private String trade_type;
    private String notify_url;

    public FwPrepayBean() {
        this.appid = FwConstUtil.FW_APP_ID;
        this.mch_id = FwConstUtil.FW_MCH_ID;
        this.mch_key = FwConstUtil.FW_PARTNER_KEY;
        this.nonce_str = BasicUtil.getNonceStr();
        this.out_trade_no = FwPayUtil.getOrderNumber(StringUtils.EMPTY);
        this.openid = StringUtils.EMPTY;
        this.body = StringUtils.EMPTY;
        this.total_fee = StringUtils.EMPTY;
        this.spbill_create_ip = StringUtils.EMPTY;
        this.trade_type = FwConstUtil.TRADE_TYPE;
        this.notify_url = StringUtils.EMPTY;
    }

    public FwPrepayBean(String openid, String body, Integer totalFee, String userIp, String notifyUrl, String outTradeNoPreFix) {
        this.appid = FwConstUtil.FW_APP_ID;
        this.mch_id = FwConstUtil.FW_MCH_ID;
        this.mch_key = FwConstUtil.FW_PARTNER_KEY;
        this.nonce_str = BasicUtil.getNonceStr();
        this.out_trade_no = FwPayUtil.getOrderNumber(outTradeNoPreFix);
        this.openid = openid;
        this.body = body;
        this.total_fee = String.valueOf(totalFee);
        this.spbill_create_ip = userIp;
        this.trade_type = FwConstUtil.TRADE_TYPE;
        this.notify_url = notifyUrl;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getAppid() {
        return appid;
    }

    public String getOpenid() {
        return openid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public String getBody() {
        return body;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public String getMch_key() {
        return mch_key;
    }

    public void setMch_key(String mch_key) {
        this.mch_key = mch_key;
    }
}
