package com.suyeer.fastwechat.bean.fwpay;

import com.alibaba.fastjson.JSON;

/**
 * @author jun 2019/3/21
 */
public class FwMchPayResult extends FwCodeAndMsgResult {
    private String mch_appid;
    private String mchid;
    private String nonce_str;
    private String partner_trade_no;
    private String payment_no;
    private String payment_time;

    public String getMch_appid() {
        return mch_appid;
    }

    public void setMch_appid(String mch_appid) {
        this.mch_appid = mch_appid;
    }

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getPartner_trade_no() {
        return partner_trade_no;
    }

    public void setPartner_trade_no(String partner_trade_no) {
        this.partner_trade_no = partner_trade_no;
    }

    public String getPayment_no() {
        return payment_no;
    }

    public void setPayment_no(String payment_no) {
        this.payment_no = payment_no;
    }

    public String getPayment_time() {
        return payment_time;
    }

    public void setPayment_time(String payment_time) {
        this.payment_time = payment_time;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
