package com.suyeer.fastwechat.bean.fwpay;

import com.alibaba.fastjson.JSON;
import com.suyeer.basic.util.BasicUtil;
import com.suyeer.fastwechat.util.FwConstUtil;
import com.suyeer.fastwechat.util.FwPayUtil;
import org.apache.commons.lang3.StringUtils;

import static com.suyeer.fastwechat.util.FwConstUtil.FW_APP_ID;

/**
 * 微信退款bean
 *
 * @author jun 2019/3/25
 */
public class FwRefundBean {
    private String appid;
    private String mch_id;
    private String mch_key;
    private String nonce_str;
    private String transaction_id;
    private String out_refund_no;
    private Integer total_fee;
    private Integer refund_fee;
    private String refund_desc;
    private String notify_url;

    public FwRefundBean() {
        this.appid = FW_APP_ID;
        this.mch_id = FwConstUtil.FW_MCH_ID;
        this.mch_key = FwConstUtil.FW_PARTNER_KEY;
        this.nonce_str = BasicUtil.getNonceStr();
        this.out_refund_no = FwPayUtil.getOrderNumber(StringUtils.EMPTY);
    }

    public FwRefundBean(String transactionId, String refundDesc, Integer totalFee, Integer refundFee, String notifyUrl, String outRefundNoPreFix) {
        this.appid = FW_APP_ID;
        this.mch_id = FwConstUtil.FW_MCH_ID;
        this.mch_key = FwConstUtil.FW_PARTNER_KEY;
        this.transaction_id = transactionId;
        this.nonce_str = BasicUtil.getNonceStr();
        this.notify_url = notifyUrl;
        this.out_refund_no = FwPayUtil.getOrderNumber(outRefundNoPreFix);
        this.refund_desc = refundDesc;
        this.total_fee = totalFee;
        this.refund_fee = refundFee;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getMch_key() {
        return mch_key;
    }

    public void setMch_key(String mch_key) {
        this.mch_key = mch_key;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public void setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
    }

    public Integer getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(Integer total_fee) {
        this.total_fee = total_fee;
    }

    public Integer getRefund_fee() {
        return refund_fee;
    }

    public void setRefund_fee(Integer refund_fee) {
        this.refund_fee = refund_fee;
    }

    public String getRefund_desc() {
        return refund_desc;
    }

    public void setRefund_desc(String refund_desc) {
        this.refund_desc = refund_desc;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
