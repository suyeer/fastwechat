package com.suyeer.fastwechat.bean.fwtemplate;

/**
 * 模版消息字段实体类
 *
 * @author jun 2018/11/22
 */
public class FwTemplateValueBean {
    private String value;
    private String color;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
