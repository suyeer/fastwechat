package com.suyeer.fastwechat.bean.fwtemplate;

/**
 * @author jun 2019/9/28
 */
public class FwTemplateMiniprogramBean {
    private String appid;
    private String pagepath;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPagepath() {
        return pagepath;
    }

    public void setPagepath(String pagepath) {
        this.pagepath = pagepath;
    }
}
