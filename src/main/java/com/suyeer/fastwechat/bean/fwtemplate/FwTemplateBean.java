package com.suyeer.fastwechat.bean.fwtemplate;

import java.util.HashMap;

/**
 * @author jun 2018/11/22
 */
public class FwTemplateBean {
    private String touser;
    private String template_id;
    private String url;
    private FwTemplateMiniprogramBean miniprogram;
    private HashMap<String, FwTemplateValueBean> data = new HashMap<String, FwTemplateValueBean>();

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String templateId) {
        template_id = templateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, FwTemplateValueBean> getData() {
        return data;
    }

    public void setData(HashMap<String, FwTemplateValueBean> data) {
        this.data = data;
    }

    public FwTemplateMiniprogramBean getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(FwTemplateMiniprogramBean miniprogram) {
        this.miniprogram = miniprogram;
    }

    public void setMiniprogramConfig(String appid, String pagepath) {
        this.miniprogram = new FwTemplateMiniprogramBean();
        this.miniprogram.setAppid(appid);
        this.miniprogram.setPagepath(pagepath);
    }

    public void addItem(String key, String value, String color) {
        FwTemplateValueBean v = new FwTemplateValueBean();
        v.setValue(value);
        v.setColor(color);
        data.put(key, v);
    }

    public void addItem(String key, String value) {
        FwTemplateValueBean v = new FwTemplateValueBean();
        v.setValue(value);
        v.setColor("#4e72b8");
        data.put(key, v);
    }
}
