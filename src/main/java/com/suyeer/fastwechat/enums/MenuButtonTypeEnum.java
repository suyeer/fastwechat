package com.suyeer.fastwechat.enums;

/**
 * @author jun 2018/11/22
 */
public enum MenuButtonTypeEnum {

    /**
     * 点击推事件
     */
    click("click", "点击推事件"),

    /**
     * 跳转URL
     */
    view("view", "跳转URL"),

    /**
     * 扫码推事件
     */
    scancode_push("scancode_push", "扫码推事件"),

    /**
     * 扫码推事件且弹出'消息接收中'提示框
     */
    scancode_waitmsg("scancode_waitmsg", "扫码推事件且弹出'消息接收中'提示框"),

    /**
     * 弹出系统拍照发图
     */
    pic_sysphoto("pic_sysphoto", "弹出系统拍照发图"),

    /**
     * 弹出拍照或者相册发图
     */
    pic_photo_or_album("pic_photo_or_album", "弹出拍照或者相册发图"),

    /**
     * 弹出微信相册发图器
     */
    pic_weixin("pic_weixin", "弹出微信相册发图器"),

    /**
     * 弹出地理位置选择器
     */
    location_select("location_select", "弹出地理位置选择器"),

    /**
     * 下发消息（除文本消息)
     */
    media_id("media_id", "下发消息（除文本消息)"),

    /**
     * 跳转图文消息URL
     */
    view_limited("view_limited", "跳转图文消息URL");

    private final String code;
    private final String des;

    MenuButtonTypeEnum(String code, String des) {
        this.code = code;
        this.des = des;
    }

    public String getCode() {
        return code;
    }
    public String getDes() {
        return des;
    }
}
