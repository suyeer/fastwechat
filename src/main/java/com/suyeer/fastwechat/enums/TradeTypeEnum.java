package com.suyeer.fastwechat.enums;

/**
 * @author jun 2018/11/22
 */
public enum TradeTypeEnum {

    /**
     * JSAPI支付（或小程序支付）
     */
    JSAPI("JSAPI支付（或小程序支付）"),

    /**
     * H5支付
     */
    MWEB("H5支付"),

    /**
     * MICROPAY
     */
    MICROPAY("MICROPAY"),

    /**
     * Native支付
     */
    NATIVE("Native支付"),

    /**
     * app支付
     */
    APP("app支付");

    private final String des;

    TradeTypeEnum(String des) {
        this.des = des;
    }

    public String getDes() {
        return des;
    }

}
