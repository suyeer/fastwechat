package com.suyeer.fastwechat.enums;

/**
 * @author jun 2018/11/22
 */
public enum LanguageEnum {

    /**
     * 简体
     */
    zh_CN("zh_CN", "简体"),

    /**
     * 繁体
     */
    zh_TW("zh_TW", "繁体"),

    /**
     * 英语
     */
    en("en", "英语");

    private final String code;
    private final String des;

    LanguageEnum(String code, String des) {
        this.code = code;
        this.des = des;
    }

    public String getCode() {
        return code;
    }

    public String getDes() {
        return des;
    }

}
