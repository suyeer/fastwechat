package com.suyeer.fastwechat.enums;

/**
 * @author jun 2018/11/22
 */
public enum CacheLocationEnum {
    /**
     * Don't use cache
     */
    NO_CACHE("Don't use cache"),

    /**
     * Use localhost server
     */
    LOCALHOST("Use localhost server"),

    /**
     * Use memcached
     */
    MEMCACHED("Use memcached"),

    /**
     * Use redis
     */
    REDIS("Use redis"),

    /**
     * Use UDP
     */
    UDP("Use UDP");

    private final String des;

    CacheLocationEnum(String des) {
        this.des = des;
    }

    public String getDes() {
        return des;
    }
}
