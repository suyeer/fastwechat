package com.suyeer.fastwechat.module;

import com.suyeer.fastwechat.bean.fwcommon.CacheBean;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author jun 2018/11/22
 */
public class WeChatDataCache {
    private static class LazyHolder {
        private static final WeChatDataCache INSTANCE = new WeChatDataCache();
    }

    public static WeChatDataCache getInstance() {
        return WeChatDataCache.LazyHolder.INSTANCE;
    }

    private WeChatDataCache() {
    }

    /**
     * 微信缓存, 如 accessToken JsApiTicket
     */
    private final ConcurrentMap<String, CacheBean> weChatCacheMap = new ConcurrentHashMap<>();

    public void add(String key, String value, Integer expiredIn) {
        CacheBean cacheBean = new CacheBean(key, value, expiredIn);
        weChatCacheMap.put(key, cacheBean);
    }

    public CacheBean get(String key) {
        return weChatCacheMap.containsKey(key) ? weChatCacheMap.get(key) : null;
    }

}

