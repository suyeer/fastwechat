package com.suyeer.fastwechat.util;

import com.alibaba.fastjson.JSONObject;
import com.suyeer.basic.util.HttpResUtil;
import com.suyeer.basic.util.JsonUtil;
import com.suyeer.fastwechat.bean.fwmenu.FwMenuBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jun 2018/11/22
 */
public class FwMenuUtil {
    private static Logger logger = LoggerFactory.getLogger(FwMenuUtil.class);

    public static JSONObject createMenuButton(FwMenuBean fwMenuBean) {
        JSONObject retJson = new JSONObject();
        try {
            String url = FwConstUtil.WX_URL_CREATE_MENU + FwUtil.getAccessToken();
            retJson = HttpResUtil.sendSSLPostRequest(url, JsonUtil.toString(fwMenuBean));
        } catch (Exception e) {
            logger.error("发送请求异常: {}", e.getMessage());
        }
        return retJson;
    }

}
