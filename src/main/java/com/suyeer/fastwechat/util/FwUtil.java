package com.suyeer.fastwechat.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.suyeer.fastwechat.bean.fwpay.FwPrepayBean;
import com.suyeer.fastwechat.bean.fwpay.FwQueryOrderResult;
import com.suyeer.fastwechat.bean.fwtemplate.FwTemplateBean;
import com.suyeer.fastwechat.enums.LanguageEnum;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;

import static org.apache.commons.codec.CharEncoding.UTF_8;

/**
 * @author jun 2018/11/22
 */
public class FwUtil {

    /**
     * 获取全局AccessToken;
     *
     * @return String
     */
    public static String getAccessToken() {
        return FwTokenUtil.getAccessToken();
    }

    /**
     * 获取JsApiTicket;
     *
     * @return String
     */
    public static String getJsApiTicket() {
        return FwTokenUtil.getJsApiTicket();
    }

    /**
     * 用户授权相关, 根据code换取信息
     *
     * @param code String
     * @return JSONObject
     */
    public static JSONObject getAuthAccessToken(String code) {
        return FwUserUtil.getAuthAccessToken(code);
    }

    /**
     * 用户授权相关, 刷新Token
     *
     * @param refreshToken String
     * @return JSONObject
     */
    public static JSONObject getRefreshToken(String refreshToken) {
        return FwUserUtil.getRefreshToken(refreshToken);
    }

    /**
     * 用户授权相关, 高级授权获取用户详细信息
     *
     * @param accessToken String
     * @param openId      String
     * @return JSONObject
     */
    public static JSONObject getAuthUserInfoByOath2Api(String accessToken, String openId) {
        return FwUserUtil.getAuthUserInfoByOath2Api(accessToken, openId);
    }

    /**
     * 根据openId获取用户信息, 需要用户关注公众号; 默认中文信息;
     *
     * @param openId String
     * @return JSONObject
     */
    public static JSONObject getUserInfoByOpenId(String openId) {
        return FwUserUtil.getUserInfoByOpenId(openId, LanguageEnum.zh_CN);
    }

    /**
     * 根据openId获取用户信息, 需要用户关注公众号;
     *
     * @param openId   String
     * @param language LanguageEnum
     * @return JSONObject
     */
    public static JSONObject getUserInfoByOpenId(String openId, LanguageEnum language) {
        return FwUserUtil.getUserInfoByOpenId(openId, language);
    }

    /**
     * 批量获取用户openid,一次可获取10000条
     *
     * @return JSONObject
     */
    public static JSONObject getUserOpenIdList(String nextOpenid) {
        return FwUserUtil.getUserOpenIdList(nextOpenid);
    }

    /**
     * 批量获取用户信息,一次可获取100条
     *
     * @param openIdList List
     * @return JSONObject
     */
    public static JSONArray getBatchUserInfo(List<String> openIdList) {
        return FwUserUtil.getBatchUserInfo(openIdList);
    }

    /**
     * 发送模板消息;
     *
     * @param fwTemplate FwTemplateBean
     * @return JSONObject
     */
    public static JSONObject sendTemplate(FwTemplateBean fwTemplate) {
        return FwTemplateUtil.sendTemplate(fwTemplate);
    }

    /**
     * 获取 wx.config() 的参数
     *
     * @param request HttpServletRequest
     * @return JSONObject
     */
    public static JSONObject getJsSdkConfig(HttpServletRequest request) {
        return FwTokenUtil.getJsSdkConfig(request);
    }

    /**
     * 获取 wx.config() 的参数, 需完整的url, 包含参数, 但不包含 # 号后面的数据;
     *
     * @param url String
     * @return JSONObject
     */
    public static JSONObject getJsSdkConfig(String url) {
        return FwTokenUtil.getJsSdkConfig(url);
    }

    /**
     * 获取微信支付数据
     *
     * @param fwPrepayBean FwPrepayBean
     * @return JSONObject
     * @throws Exception Exception
     */
    public static JSONObject getPayParams(FwPrepayBean fwPrepayBean) throws Exception {
        return FwPayUtil.getPayParams(fwPrepayBean);
    }

    /**
     * 获取微信支付订单号
     *
     * @param preFixString String
     * @return String
     */
    public static String getOrderNumber(String preFixString) {
        return FwPayUtil.getOrderNumber(preFixString);
    }

    /**
     * 检查是否是来自微信的请求
     *
     * @param paramJsonStr String
     * @param token        String
     * @return String
     * @throws Exception Exception
     */
    public static String checkWxRequest(String paramJsonStr, String token) throws Exception {
        return FwBaseUtil.checkWxRequest(paramJsonStr, token);
    }

    public static String getMediaUrl(String mediaId) {
        return FwMediaUtil.getMediaUrl(mediaId);
    }

    public static String getSpeexMediaUrl(String mediaId) {
        return FwMediaUtil.getSpeexMediaUrl(mediaId);
    }

    public static InputStream getMediaInputStream(String mediaId) {
        return FwMediaUtil.getMediaInputStream(mediaId);
    }

    public static InputStream getSpeexMediaInputStream(String mediaId) {
        return FwMediaUtil.getSpeexMediaInputStream(mediaId);
    }

    public static void saveMediaFile(String mediaId, String localFilePath) {
        FwMediaUtil.saveMediaFile(mediaId, localFilePath);
    }

    public static void saveSpeexMediaFile(String mediaId, String localFilePath) {
        FwMediaUtil.saveSpeexMediaFile(mediaId, localFilePath);
    }

    public static String getPaySuccessNoticeReturnXmlContent() {
        return FwPayUtil.getPaySuccessNoticeReturnXmlContent();
    }

    public static String getPayFailNoticeReturnXmlContent() {
        return FwPayUtil.getPayFailNoticeReturnXmlContent();
    }

    public static FwQueryOrderResult checkPayBackNotify(InputStream is) throws Exception {
        return FwPayUtil.checkPayBackNotify(IOUtils.toString(is, UTF_8));
    }

    public static FwQueryOrderResult checkPayBackNotify(String notifyContent) throws Exception {
        return FwPayUtil.checkPayBackNotify(notifyContent);
    }

}
