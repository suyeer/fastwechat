package com.suyeer.fastwechat.util;

import com.alibaba.fastjson.JSONObject;
import com.suyeer.basic.util.HttpResUtil;
import com.suyeer.basic.util.JsonUtil;
import com.suyeer.fastwechat.bean.fwtemplate.FwTemplateBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.suyeer.fastwechat.util.FwConstUtil.WX_URL_SEND_TEMPLATE_MESSAGE;

/**
 * 模板消息方法
 *
 * @author jun 2018/11/22
 */
public class FwTemplateUtil {
    private static Logger logger = LoggerFactory.getLogger(FwMenuUtil.class);

    /**
     * 发送模板消息
     * @param fwTemplateBean FwTemplateBean
     * @return JSONObject
     */
    public static JSONObject sendTemplate(FwTemplateBean fwTemplateBean) {
        JSONObject retObj = new JSONObject();
        try {
            retObj = HttpResUtil.sendSSLPostRequest(WX_URL_SEND_TEMPLATE_MESSAGE + FwUtil.getAccessToken(), JsonUtil.toString(fwTemplateBean));
        } catch (Exception e) {
            logger.error("发送请求异常: {}", e.getMessage());
        }
        return retObj;
    }
}
