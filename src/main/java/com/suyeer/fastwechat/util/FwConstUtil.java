package com.suyeer.fastwechat.util;

import com.suyeer.fastwechat.bean.base.WeChatProperties;
import com.suyeer.fastwechat.enums.CacheLocationEnum;

/**
 * @author jun 2018/11/22
 */
public class FwConstUtil {

    /**
     * 微信公众号参数配置
     */
    public final static String FW_APP_ID = WeChatProperties.getInstance().getAppId();
    public final static String FW_APP_SECRET = WeChatProperties.getInstance().getAppSecret();
    public final static String FW_MCH_ID = WeChatProperties.getInstance().getMchId();
    public final static String FW_PARTNER_KEY = WeChatProperties.getInstance().getPartnerKey();
    public final static String FW_DOMAIN = WeChatProperties.getInstance().getDomain();
    public final static CacheLocationEnum FW_CACHE_LOCATION = WeChatProperties.getInstance().getCacheLocation();
    public final static int TICKET_CACHE_REFRESH_TIME = WeChatProperties.getInstance().getTicketCacheRefreshTime();
    public final static int ACCESS_TOKEN_ACTIVE_TIME = WeChatProperties.getInstance().getAccessTokenActiveTime();

    /**
     * common
     */
    public final static String DEFAULT_ENCODE = "UTF-8";
    public final static String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    public final static String CONTENT_TYPE_APPLICATION_XML = "application/xml";
    public final static String CONTENT_TYPE_APPLICATION_JSONP = "application/x-json";
    public final static String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
    public final static String ERROR = "### ERROR IN FASTWECHAT ###";
    public final static String KEY_CACHE_WX_ACCESS_TOKEN = WeChatProperties.getInstance().getCachePrefixKey() + "KEY_CACHE_WX_ACCESS_TOKEN";

    /**
     * 微信开发数据
     */
    public final static String WX_URL_CGI_BIN_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + FW_APP_ID + "&secret=" + FW_APP_SECRET;
    public final static String WX_URL_GET_JS_API_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=";
    public final static String WX_URL_GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=%s";
    public final static String WX_URL_UNIFIED_ORDER = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    public final static String WX_URL_ORDER_QUERY = "https://api.mch.weixin.qq.com/pay/orderquery";
    public final static String WX_URL_MCH_PAY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
    public final static String WX_URL_GET_MEDIA_FILE = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";
    public final static String WX_URL_GET_SPEEX_MEDIA_FILE = "https://api.weixin.qq.com/cgi-bin/media/get/jssdk?access_token=%s&media_id=%s";
    public final static String WX_KEY_ACCESS_TOKEN = WeChatProperties.getInstance().getCachePrefixKey() + "WX_KEY_ACCESS_TOKEN";
    public final static String WX_KEY_JS_API_TICKET = WeChatProperties.getInstance().getCachePrefixKey() + "WX_KEY_JS_API_TICKET";
    public final static String WX_PARAM_SIGNATURE = "signature";
    public final static String WX_PARAM_ECHO_STR = "echostr";
    public final static String WX_PARAM_ACCESS_TOKEN = "access_token";
    public final static String WX_PARAM_TICKET = "ticket";
    public final static String WX_PARAM_TIMESTAMP = "timestamp";
    public final static String WX_PARAM_NONCE = "nonce";
    public final static String WX_RESPONSE_STATUS_SUCCESS = "SUCCESS";
    public final static String H5_TRADE_TYPE = "MWEB";
    public final static String TRADE_TYPE = "JSAPI";

    /**
     * 菜单工具
     */
    public final static String WX_URL_CREATE_MENU = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";

    /**
     * 模板消息
     */
    public final static String WX_URL_SEND_TEMPLATE_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";

    /**
     * 用户相关
     */
    public final static String WX_URL_OAUTH2_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + FW_APP_ID + "&secret=" + FW_APP_SECRET + "&grant_type=authorization_code&code=";
    public final static String WX_URL_OAUTH2_REFRESH_TOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + FW_APP_ID + "&grant_type=refresh_token&refresh_token=";
    public final static String WX_URL_OAUTH2_GET_USER_INFO = "https://api.weixin.qq.com/sns/userinfo?lang=zh_CN&access_token=%s&openid=%s";
    public final static String WX_URL_GET_USER_LIST_OPENID = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=";
    public final static String WX_URL_GET_BATCH_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=";


}
