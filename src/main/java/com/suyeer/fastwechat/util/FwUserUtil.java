package com.suyeer.fastwechat.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.suyeer.basic.util.HttpResUtil;
import com.suyeer.fastwechat.enums.LanguageEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.suyeer.fastwechat.util.FwConstUtil.*;

/**
 * @author jun 2018/11/22
 */
public class FwUserUtil {

    private static Logger logger = LoggerFactory.getLogger(FwUserUtil.class);

    /**
     * 用户授权相关, 根据code换取信息
     *
     * @param code String
     * @return JSONObject
     */
    public static JSONObject getAuthAccessToken(String code) {
        String url = WX_URL_OAUTH2_ACCESS_TOKEN + code;
        return sendRequest(url);
    }

    /**
     * 用户授权相关, 刷新Token
     *
     * @param refreshToken String
     * @return JSONObject
     */
    public static JSONObject getRefreshToken(String refreshToken) {
        String url = WX_URL_OAUTH2_REFRESH_TOKEN + refreshToken;
        return sendRequest(url);
    }

    /**
     * 用户授权相关, 高级授权获取用户详细信息
     *
     * @param accessToken String
     * @param openId      String
     * @return JSONObject
     */
    public static JSONObject getAuthUserInfoByOath2Api(String accessToken, String openId) {
        String url = String.format(WX_URL_OAUTH2_GET_USER_INFO, accessToken, openId);
        return sendRequest(url);
    }

    /**
     * 根据openId获取用户信息, 需要用户关注公众号; 默认中文信息;
     *
     * @param openId String
     * @return JSONObject
     */
    public static JSONObject getUserInfoByOpenId(String openId) {
        return getUserInfoByOpenId(openId, LanguageEnum.zh_CN);
    }

    /**
     * 根据openId获取用户信息, 需要用户关注公众号;
     *
     * @param openId   String
     * @param language LanguageEnum
     * @return JSONObject
     */
    public static JSONObject getUserInfoByOpenId(String openId, LanguageEnum language) {
        String url = String.format(FwConstUtil.WX_URL_GET_USER_INFO, FwUtil.getAccessToken(), openId, language.getCode());
        return getUserInfo(url);
    }

    /**
     * 获取用户信息
     *
     * @param url String
     * @return JSONObject
     */
    private static JSONObject getUserInfo(String url) {
        return sendRequest(url);
    }

    /**
     * 批量获取用户openid,一次可获取10000条
     *
     * @param nextOpenid 可以设置为null, 此时从头开始获取数据
     * @return JSONObject
     */
    public static JSONObject getUserOpenIdList(String nextOpenid) {
        String paramStr = StringUtils.isBlank(nextOpenid) ? "" : "?next_openid=" + nextOpenid;
        String url = WX_URL_GET_USER_LIST_OPENID + FwUtil.getAccessToken() + paramStr;
        return sendRequest(url);
    }

    /**
     * 批量获取用户信息,一次可获取100条
     *
     * @param params JSONObject
     * @return JSONObject
     */
    public static JSONArray getBatchUserInfo(JSONObject params) {
        String url = WX_URL_GET_BATCH_USER_INFO + FwUtil.getAccessToken();
        String retStr = HttpResUtil.sendHttpPostRequest(url, params.toJSONString());
        JSONObject obj = JSON.parseObject(retStr);
        return obj.getJSONArray("user_info_list");
    }

    /**
     * 批量获取用户信息,一次可获取100条
     *
     * @param openIdList List
     * @return JSONObject
     */
    public static JSONArray getBatchUserInfo(List<String> openIdList) {
        return getBatchUserInfo(openIdList, LanguageEnum.zh_CN);
    }

    /**
     * 批量获取用户信息,一次可获取100条
     *
     * @param openIdList List
     * @return JSONObject
     */
    public static JSONArray getBatchUserInfo(List<String> openIdList, LanguageEnum language) {
        JSONObject params = new JSONObject();
        JSONArray array = new JSONArray();
        params.put("user_list", array);
        for (String s : openIdList) {
            JSONObject object = new JSONObject();
            object.put("openid", s);
            object.put("lang", language.getCode());
            array.add(object);
        }
        return getBatchUserInfo(params);
    }


    /**
     * 发送请求
     *
     * @param url String
     * @return JSONObject
     */
    private static JSONObject sendRequest(String url) {
        JSONObject retObj = null;
        try {
            retObj = HttpResUtil.sendHttpGetRequest(url);
        } catch (Exception e) {
            logger.error("### ERROR IN FASTWECHAT ### 请求微信接口失败: {}", e.getMessage());
        }
        return retObj;
    }
}
