# 微信公众号开工具框架
### 项目简介
[fastwechat](https://git.oschina.net/suyeer/fastwechat) 是针对微信公众号开发集成的工具框架, 项目内部完成与微信服务器的交互, 以实现web项目快速接入微信的需求. 本项目会根据开发进度不定时的发送新版本的jar包到maven中央服务器. 欢迎加入! 

### 框架使用
1. 引入框架;
2. 配置spring bean;
- 参数详解: [WeChatProperties](https://gitee.com/suyeer/fastwechat/blob/master/src/main/java/com/suyeer/fastwechat/bean/base/WeChatProperties.java) 
- 配置样例: [fastWeChat.xml](https://git.oschina.net/suyeer/codes/zyat4rqm3njh2u5fo7dlp26);
- 负载均衡需配置: [memcached.xml](http://git.oschina.net/suyeer/cache/wikis/config);

### maven最新版依赖
```
<dependency>
    <groupId>com.suyeer</groupId>
    <artifactId>fastwechat</artifactId>
    <version>3.4.0</version>
</dependency>
```
### 开发相关
- idea + tomcat7 + jdk1.8 + maven *必须
- [TeamCity](https://www.jetbrains.com/teamcity/) 持续集成插件 <font color=#00ffff>*必须</font>
- [码云账号](https://git.oschina.net/) git代码托管 *必须
- [nginx](http://nginx.org) 反向代理服务器 *了解
- [Nexus](http://www.sonatype.org/nexus/) Maven仓库管理器 *了解
- [redis](https://redis.io/) 常用高效缓存 *了解
- [memcached](http://memcached.org) 常用高效缓存 *了解

### 任务简介

- 消息管理
- 微信网页开发
- 用户管理
- 微信支付接入
- [more...](https://mp.weixin.qq.com/wiki/home/index.html)

### 联系我
- 邮箱 : 1951227536@qq.com

![微信](http://git.oschina.net/uploads/images/2017/0113/102353_4a3222e9_761996.png "我的微信")![钉钉](http://git.oschina.net/uploads/images/2017/0113/102420_e532e517_761996.png "我的钉钉")

### 最后
本项目开源, 感谢加入我们的小伙伴贡献代码 !